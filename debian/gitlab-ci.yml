variables:
  DEBFULLNAME: "Salsa Pipeline"
  DEBEMAIL: "<salsa-pipeline@debian.org>"
  DEBIAN_FRONTEND: noninteractive
  WORKING_DIR: ./debian/output
  DEB_BUILD_OPTIONS: "nocheck noautodbgsym"
  RELEASE: unstable

stages:
  - build
  - test

build package:
  stage: build
  image: registry.salsa.debian.org/salsa-ci-team/images/gbp
  services:
    - docker:dind
  artifacts:
    expire_in: 180 day
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/
  script:
    - gbp pull --ignore-branch --pristine-tar --track-missing
    - while true; do sleep 600; echo "10 minutes passed" >&2; done & # Progress keeper since build is long and silent
    - gbp buildpackage --git-ignore-branch --git-export-dir=${WORKING_DIR} -us -uc --git-builder="docker-build.sh registry.salsa.debian.org/salsa-ci-team/images/dockerbuilder:${RELEASE}" | tail -n 10000 # Keep log under 4 MB
    - du -shc ${WORKING_DIR}/* # Show total file size of artifacts. Must stay are under 100 MB.

# Autopkgtest must be disabled, because the test 'upstream' will be skipped and
# thus the command returns exit code 2 and is considered failed by Gitlab CI.
# Removing 'breaks-testbed' stanza from tests/control will result in that the
# test is run on Gitlab CI, but then it will fail in multiple tests as it cannot
# bind to network interfaces and other needes stuff inside Gitlab CI containers.
#
#run autopkgtest:
#  stage: test
#  script:
#    - apt-get update && apt-get install autopkgtest eatmydata -y --no-install-recommends
#    - eatmydata autopkgtest ${WORKING_DIR}/*.deb -- null

run lintian:
  stage: test
  image: registry.salsa.debian.org/salsa-ci-team/images/lintian
  script:
    - apt-get update
    - apt-get install -y dpkg-dev
    - lintian -iI ${WORKING_DIR}/*.changes

# Reprotest exceeds the 2 hour timeout limit on Github CI, so results in failure
# and must be disabled until a quicker (times two) build is possible.
#run reprotest:
#  stage: test
#  image: genericpipeline/reprotest-docker
#  artifacts:
#    name: "$CI_BUILD_NAME"
#    expire_in: 180 day
#    paths:
#      - ./reprotest.log
#    when: always
#  script:
#    - apt-get update && apt-get install eatmydata -y
#    - eatmydata apt-get build-dep -y .
#    - export DEB_BUILD_OPTIONS=nocheck
#    - eatmydata reprotest . -- null &> reprotest.log
#  tags:
#    - privileged

# Piuparts is passing on official piuparts.debian.org, but fail on Gitlab CI
# with 'FAIL: Package purging left files on system'. Disabling for now.
#run piuparts:
#  stage: test
#  image: genericpipeline/piuparts-docker
#  services:
#    - docker:dind
#  script:
#    - CHROOT_PATH=/tmp/debian-unstable
#    - CONTAINER_ID=$(docker run --rm -d debian:unstable sleep infinity)
#    - docker exec ${CONTAINER_ID} bash -c "apt-get update && apt-get install eatmydata -y"
#    - mkdir -p ${CHROOT_PATH}
#    - docker export ${CONTAINER_ID} | tar -C ${CHROOT_PATH} -xf -
#    - mknod -m 666 ${CHROOT_PATH}/dev/urandom c 1 9
#    - piuparts --hard-link -e ${CHROOT_PATH} ${WORKING_DIR}/*.deb
#  tags:
#    - privileged

test install:
  stage: test
  image: debian:sid
  artifacts:
    when: always
    expire_in: 180 day
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - mkdir debug
    - dpkg -l | grep -iE '"'"'maria|mysql|galera'"'"' || true
    - apt-get update
    - apt-get install -y ./*.deb
    - mariadb --version
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - service mysql restart || true # Always proceed even if init failed
    - service mysql status || true
    - cp -ra /var/log/mysql debug/var-log-mysql
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - mariadb -e "create database test; use test; create table t(a int primary key) engine=innodb; insert into t values (1); select * from t; drop table t; drop database test;"

test upgrade:
  stage: test
  image: debian:sid
  artifacts:
    when: always
    expire_in: 180 day
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - mkdir debug
    - dpkg -l | grep -iE '"'"'maria|mysql|galera'"'"' || true
    - apt-get update
    - apt-get install -y mariadb-server-10.1 # Debian unstable still has this and other -10.1 packages
    - service mysql status || true # Always proceed even if init failed
    - service mysql restart || true
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - apt-get install -y ./*.deb
    - mariadb --version
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - service mysql restart || true # Always proceed even if init failed
    - service mysql status || true
    - cp -ra /var/log/mysql debug/var-log-mysql
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - mariadb -e "create database test; use test; create table t(a int primary key) engine=innodb; insert into t values (1); select * from t; drop table t; drop database test;"

# Upgrading from MySQL 5.7 does not yet fully work due to differences in
# auth-socket plugin name and Password column in user table.
#test mysql upgrade:
#  stage: test
#  image: debian:sid
#  script:
#    - apt-get update
#    - apt-get install -y mysql-server
#    - /etc/init.d/mysql restart
#    - mysql --version
#    - echo 'SHOW DATABASES;' | mysql
#    - cd ${WORKING_DIR}; apt-get install -y ./*.deb
#    - /etc/init.d/mysql restart
#    - mariadb --version
#    - echo 'SHOW DATABASES;' | mariadb

test features:
  stage: test
  image: debian:sid
  artifacts:
    when: always
    expire_in: 180 day
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - mkdir debug
    - dpkg -l | grep -iE '"'"'maria|mysql|galera'"'"' || true
    - apt-get update
    - apt-get install -y ./*.deb
    - mariadb --version
    - find /var/lib/mysql -ls > debug/var-lib-mysql.list
    - cp -ra /etc/mysql debug/etc-mysql
    - service mysql restart || true # Always proceed even if init failed
    - service mysql status || true
    - cp -ra /var/log/mysql debug/var-log-mysql
    # Print info about server
    - mariadb --skip-column-names -e "select @@version, @@version_comment"
    - mariadb --skip-column-names -e "select engine, support, transactions, savepoints from information_schema.engines order by engine" | sort
    - mariadb --skip-column-names -e "select plugin_name, plugin_status, plugin_type, plugin_library, plugin_license from information_schema.all_plugins order by plugin_name, plugin_library"
    # Test various features
    - mariadb -e "CREATE DATABASE db"
    - mariadb -e "CREATE TABLE db.t_innodb(a1 SERIAL, c1 CHAR(8)) ENGINE=InnoDB; INSERT INTO db.t_innodb VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_myisam(a2 SERIAL, c2 CHAR(8)) ENGINE=MyISAM; INSERT INTO db.t_myisam VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_aria(a3 SERIAL, c3 CHAR(8)) ENGINE=Aria; INSERT INTO db.t_aria VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE TABLE db.t_memory(a4 SERIAL, c4 CHAR(8)) ENGINE=MEMORY; INSERT INTO db.t_memory VALUES (1,'"'"'foo'"'"'),(2,'"'"'bar'"'"')"
    - mariadb -e "CREATE ALGORITHM=MERGE VIEW db.v_merge AS SELECT * FROM db.t_innodb, db.t_myisam, db.t_aria"
    - mariadb -e "CREATE ALGORITHM=TEMPTABLE VIEW db.v_temptable AS SELECT * FROM db.t_innodb, db.t_myisam, db.t_aria"
    - mariadb -e "CREATE PROCEDURE db.p() SELECT * FROM db.v_merge"
    - mariadb -e "CREATE FUNCTION db.f() RETURNS INT DETERMINISTIC RETURN 1"
    # Test that the features still work (this step can be done e.g. after an upgrade)
    - mariadb -e "SHOW TABLES IN db"
    - mariadb -e "SELECT * FROM db.t_innodb; INSERT INTO db.t_innodb VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_myisam; INSERT INTO db.t_myisam VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_aria; INSERT INTO db.t_aria VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT * FROM db.t_memory; INSERT INTO db.t_memory VALUES (3,'"'"'foo'"'"'),(4,'"'"'bar'"'"')"
    - mariadb -e "SELECT COUNT(*) FROM db.v_merge"
    - mariadb -e "SELECT COUNT(*) FROM db.v_temptable"
    - mariadb -e "CALL db.p()"
    - mariadb -e "SELECT db.f()"

test upgrade-libmysql:
  stage: test
  image: debian:sid
  artifacts:
    when: always
    expire_in: 180 day
    name: "$CI_BUILD_NAME"
    paths:
      - ${WORKING_DIR}/debug
  script:
    - cd ${WORKING_DIR} # Don't repeat this step, it's just cd ./debian/output
    - mkdir debug
    - dpkg -l | grep -iE "maria|mysql|galera" || true
    - apt-get update
    - apt-get install -y pkg-config libmysqld-dev libmysqlclient-dev
    - pkg-config --list-all
    - pkg-config --cflags mysqlclient # mysqlclient.pc from original package
    - apt-get install -y ./libmariadb3_*.deb ./mariadb-common_*.deb
    - pkg-config --list-all
    - apt-get install -y ./libmariadbclient18_*.deb
    - pkg-config --list-all
    - apt-get install -y ./libmariadb-dev_*.deb
    - pkg-config --list-all
    - apt-get install -y ./libmariadb-dev-compat_*.deb
    - pkg-config --cflags mysqlclient # mysqlclient.pc from compat package
    - pkg-config --list-all
    - apt-get install -y ./libmariadbd19_*.deb
    - pkg-config --list-all
    - apt-get install -y ./libmariadbd-dev_*.deb
    - pkg-config --list-all
    - apt-get install -y default-libmysqlclient-dev default-libmysqld-dev
    - pkg-config --list-all
    - pkg-config --cflags mysqlclient
    - pkg-config --cflags libmariadb
    - pkg-config --cflags mariadb
